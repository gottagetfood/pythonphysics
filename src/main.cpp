#include <math.h>
#include <SDL2/SDL.h>

#include <stdlib.h>
#include <iostream>
#include <stdio.h>

#include <unistd.h>

#include <signal.h>

#define WIDTH 640
#define HEIGHT 480


#define GRAV_DK 9.82

typedef struct {
    int size;
    double x,y;
    double forceX, forceY;
    double cor;
} Box;

void doGrav(Box* box){
    if (box->forceY < 0)
        box->forceY += GRAV_DK/1000;
    else if (box->forceY > 0)
        box->forceY += GRAV_DK/1000;
    else if (box->y != HEIGHT - box->size/2)
        box->forceY = (GRAV_DK / 1000);
    else 
        box->forceY = 0;
}

int main(){
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Event event;
    
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer);
    
    
    Box box_b = {50, WIDTH/2, HEIGHT/2, 0, -3, 0.9};
    Box* box = &box_b;
    
    int ii = 0;
    while(1){

        if(SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

        SDL_Rect rect = {box->x - (int)(box->size/2), box->y - (int)(box->size/2), box->size, box->size};

        SDL_RenderFillRect(renderer, &rect);


        if(box->y >= HEIGHT - box->size/2 && abs(box->forceY) < 0.9) {
            box->y = HEIGHT-box->size/2, box->forceY = 0;
        }
        
        doGrav(box);
        box->y += box->forceY;
        
        if(box->y + box->size/2 >= HEIGHT || box->y <= 0)
            box->forceY *= -box->cor;//, box->y = box->y >= HEIGHT ? HEIGHT - 1 : 1;
        
        SDL_RenderPresent(renderer);

        if (ii % 1000 == 0)
            printf("y: %lf, fy: %lf\n", box->y, box->forceY);

   //     usleep(1000);
        ii++;
    }



    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    
}


