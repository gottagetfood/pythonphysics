#!/bin/env python3
import time
import calculations
from graphics import *

win = GraphWin("main window", 500, 500)
loopstart = 0
starttime = time.time()

def clear(win):
    for item in win.items[:]:
        item.undraw()
    win.update()

def drawnew(thing, win):
	thing.draw(win)
	rect.draw(win)

	
while 1==1:
	rect = Rectangle(Point(0,0), Point(500,500))
	rect.setFill("white")
	looptime = loopstart-time.time()
	speed = calculations.speed(time.time()-starttime)*5
	print(time.time()-starttime)
	print(speed)
	movement = calculations.movement(looptime, speed)
	print(movement)
	x = Point(100, 100-movement*1000)
	loopstart = time.time()
	circ = Circle(x, 25)
	circ.setFill("black")
	drawnew(circ, win)