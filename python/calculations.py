from math import *
from constants import *

class ball:
    mass = 1 #kg
    radius = 0.1 #m
    height = 1 #m

def gravity(mass, g=GRAVITY_DK):
    return mass*g

def buoyancy(density, volume, g=GRAVITY_DK):
    return density*volume*g

def velocity_due_to_gravity(height, g=GRAVITY_DK):
    return sqrt(2*height/g)*g

def kinetic_energy(mass, velocity):
    return 0.5*mass*(velocity**2)

def speed(time_elapsed, g=GRAVITY_DK):
	return time_elapsed*g
	
def movement(time, velocity):
	return time*velocity
	
	
def output(objekt):
    return kinetic_energy(objekt.mass, velocity_due_to_gravity(objekt.height))



print(velocity_due_to_gravity(ball.height))
print(output(ball))

