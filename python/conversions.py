

# Pressure

def barToPa(bar):
    return bar * 100000 

def atmToPa(atm):
    return atm * 101325 

def atToPa(at):
    return at * 98070 

def mmHgToPa(mmHg):
    return mmHg * 133.3 

# Energy

def kWHToJ(kWH):
    return kWH * 3600000 

def hkToW(hk):
    return hk * 735.5 

def hpToW(hp):
    return hp * 745.7




