
all: cmake
	make -C bin/ --no-print-directory
	

cmake: 
	mkdir -p bin/
	cmake -B bin/ -S ./

run:
	./bin/PhysicsEngine


clean:
	rm -rf bin/*


